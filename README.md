# Catalogue of third-party Sardana projects

The aim of this catalogue is to facilitate the Sardana users and
developers access to the third-party Sardana projects e.g. plugins, GUIs, tools, etc.

The idea of creating such a catalogue came from the [SEP16](https://github.com/reszelaz/sardana/blob/sep16/doc/source/sep/SEP16.md).

**IMPORTANT**: The Sardana plugins projects are currently being moved from the old repositories:
[macros](https://sourceforge.net/p/sardana/macros.git/ci/master/tree/) and
[controllers](https://sourceforge.net/p/sardana/controllers.git/ci/master/tree/).
If you don't find a plugin here, take a look also at the old repos. Sorry about the inconveniences!

It is the whole Sardana Community responsibility to keep it up-to-date. So 
if you develop a Sardana related project, please consult the [CONTRIBUTING.md](CONTRIBUTING.md) guide on
how to register it here.

This catalogue groups Sardana related projects in the following categories:

* Plugin:
    * [Hardware](plugin/hardware.md)
    * [Instrument](plugin/instrument.md)
    * [System](plugin/system.md)
    * [Software](plugin/software.md)
    * [Other](plugin/other.md)
* [Other](other.md)

To find what you need simply browse the above categories or use the GitLab search within this repository for a keyword.

If you would like to be notified when new projects get registered in the
catalogue just subscribe to this repository notifications.
